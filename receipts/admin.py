from django.contrib import admin
from receipts.models import ExpenseCategory, Account, Receipt

# Register your models here.
admin.site.register(ExpenseCategory)
class ExpenseCategoryAdmin(admin.ModelAdmin):
    list_display = [
        "name",
        "id"
    ]

admin.site.register(Account)
class AccountAdmin(admin.ModelAdmin):
    list_display = [
        "name",
        "number",
        "id"
    ]

admin.site.register(Receipt)
class ReceiptAdmin(admin.ModelAdmin):
    list_display = [
        "vendor",
        "total"
        "tax",
        "date",
        "id"
    ]

def __str__(self) -> None:
    return self.title
