from django.shortcuts import render, redirect
from django.http import HttpRequest
from receipts.models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm, CategoryForm, AccountForm

# Create your views here.
@login_required
def home(request: HttpRequest):
    receipts = Receipt.objects.filter(purchaser = request.user)
    context = {
        "receipts": receipts
    }
    return render(request, "receipts/list.html", context)

@login_required
def category_list(request:HttpRequest):
    categories = ExpenseCategory.objects.filter(owner = request.user)
    context = {
        "categories": categories
    }
    return render(request, "receipts/categories/categories.html", context)

@login_required
def account_list(request:HttpRequest):
    accounts = Account.objects.filter(owner = request.user)
    context = {
        "accounts": accounts
        }
    return render(request, "receipts/accounts/accounts.html", context)

def create_receipt(request:HttpRequest):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipts = form.save(False)
            receipts.purchaser = request.user
            receipts.save()
            return redirect("home")
    else:
        form = ReceiptForm()
        context = {
            "form": form
        }
        return render(request, "receipts/create.html", context)

def create_category(request:HttpRequest):
    if request.method == "POST":
        form = CategoryForm(request.POST)
        if form.is_valid():
            categories = form.save(False)
            categories.owner = request.user
            categories.save()
            return redirect("category_list")
    else:
        form = CategoryForm()
        context = {
            "form": form
        }
        return render(request, "receipts/categories/create_categories.html", context)

def create_account(request:HttpRequest):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            accounts = form.save(False)
            accounts.owner = request.user
            accounts.save()
            return redirect("account_list")
    else:
        form = AccountForm()
        context = {
            "form": form
        }
        return render(request, "receipts/accounts/create_accounts.html", context)
